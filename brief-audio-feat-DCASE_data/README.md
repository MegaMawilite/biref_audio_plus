# Projet pour le brief sur l'audio

Projet qui contient le code nécessaire au développement du brief audio



--- Rapport:

Pour commencer, j'ai changé le modèle en un GridSearch, qui m'a retourné une prédiction de 90.
J'ai regardé quels hyperparamètres du GridSearch donnait le meilleur score et j'ai essayé des paramètres de plus en plus proches d'eux.
J'ai trouvé un résultat de 92% avec un kernel rbf avec le GridSearch, ce qui est plutôt bien.

J'ai continué à chercher en m'intérressant sur l'optimisation bayésienne.
J'ai fait comme avec le GridSearch: Cibler les meilleurs paramètres.